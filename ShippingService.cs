using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv5
{
    
    class ShippingService
    {
        //2.zadatak
        private double shippingPricePerkilo;
        public ShippingService(double shippingPricePerkilo)
        {
            this.shippingPricePerkilo = shippingPricePerkilo;
        }
        public double getShippingCost(IShipable shippable)
        {
            return shippable.Weight * this.shippingPricePerkilo;
        }
    }
}