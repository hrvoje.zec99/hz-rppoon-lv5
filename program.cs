using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv5
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.zadatak
            //Product NovaLopta = new Product("Mikasa FLL555", 20, 10);
            //Product NoviDres = new Product("mnk brod", 200, 5);
            //Product NovePatike = new Product("Mizuno MRL Sala Club Indoor", 150, 20);

            //Box box_one = new Box("box");
            //box_one.Add(NovaLopta);
            //box_one.Add(NoviDres);

            //ShippingService shipping = new ShippingService(2);
            //Console.WriteLine(shipping.getShippingCost(box_one));
            //Console.WriteLine(shipping.getShippingCost(NovePatike));

            //box_one.Add(NovePatike);
            //Console.WriteLine(shipping.getShippingCost(box_one));
            
            
            //3.zadatak
            VirtualProxyDataset virtualProxy = new VirtualProxyDataset(@"C: \Users\hrco\Desktop\zadatak3.txt");
            User Hrco = User.GenerateUser("Hrco");
            User Zec = User.GenerateUser("Zec");
            ProtectionProxyDataset protectionProxy = new ProtectionProxyDataset(Hrco);
            ProtectionProxyDataset protectionProxy_one = new ProtectionProxyDataset(Zec);

            DataConsolePrinter consolePrinter = new DataConsolePrinter();

            consolePrinter.printData(virtualProxy);
            consolePrinter.printData(protectionProxy);
            consolePrinter.printData(protectionProxy_one);

        }
    }
}