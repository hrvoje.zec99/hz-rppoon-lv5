using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv5
{
    //3.zadatak
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();

    }
}