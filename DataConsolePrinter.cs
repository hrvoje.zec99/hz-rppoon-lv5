using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv5
{
    //3.zadatak
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void printData(IDataset dataset)
        {
            if (dataset.GetData() != null)
            {
                foreach (List<string> list in dataset.GetData())
                {
                    foreach (String rezultat in list)
                    {
                        Console.Write(rezultat);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
