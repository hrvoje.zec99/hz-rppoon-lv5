using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_lv5
{
    //3.zadatak
    class ProtectionProxyDataset:IDataset
    {
        private Dataset dataset;
        private List<int> allowedIDs;
        public User user { private get; set; }
        public ProtectionProxyDataset(User user)
        {
            this.allowedIDs = new List<int>(new int[] { 1, 3, 5 });
            this.user = user;
        }
        private bool AuthenticateUser()
        {
            return allowedIDs.Contains(this.user.ID);
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if (this.AuthenticateUser())
            {
                if (this.dataset == null)
                {
                    this.dataset = new Dataset(@"C: \Users\hrco\Desktop\zadatak3.txt");
                }
                return this.dataset.GetData();
            }
            return null;
        }

        ReadOnlyCollection<List<string>> IDataset.GetData()
        {
            throw new NotImplementedException();
        }
    }
}